import mysql.connector
from mysql.connector import errorcode
import numpy

"""
Returns a dictionary containing region names mapped to a list of (x, z) polygon points stored as tuples.
"""
def points_from_db(user: str, passwd: str, host: str, db: str, poly2d_table: str, cuboid_table: str, world_id: int) -> dict:
    try:
        db = mysql.connector.connect(host=host,user=user,password=passwd,database=db)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Bad user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

        db.close()
        return -1
    
    cursor = db.cursor()
    cursor.execute("SELECT region_id, x, z FROM {table} WHERE world_id={world_id}".format(table=poly2d_table, world_id=world_id))
    
    region_data = {}

    for (region, x, z) in cursor:
        if region not in region_data:
            region_data[region] = []
        region_data[region].append((x, z))
    
    cursor.execute("SELECT region_id, min_x, min_z, max_x, max_z FROM {table} WHERE world_id={world_id}".format(table=cuboid_table, world_id=world_id))

    for (region, min_x, min_z, max_x, max_z) in cursor:
        if region in region_data:
            continue
        region_data[region] = [(min_x, min_z), (min_x, max_z), (max_x, max_z), (max_x, min_z)]

    cursor.close()
    db.close()

    return region_data

"""
Returns the area of a polygon. Shoelace algorithm lifted from https://stackoverflow.com/a/30408825/4645647
"""
def region_area(points: list) -> int:
    x, y = zip(*points)
    return 0.5*numpy.abs(numpy.dot(x,numpy.roll(y,1))-numpy.dot(y,numpy.roll(x,1)))

"""
Fetches region data from database and calculates their areas. Returns a dict mapping region name to area.
"""
def calculate_areas_from_db(user: str, passwd: str, host: str, db: str, poly2d_table: str, cuboid_table: str, world_id: str) -> dict:
    areas = {}
    polygons = points_from_db(user, passwd, host, db, poly2d_table, cuboid_table, world_id)
    if polygons == -1:
        return -1
    for region in polygons:
        areas[region] = region_area(polygons[region])
    return areas