import cherrypy
import compute

class AreaWebServer(object):
    @cherrypy.expose
    def index(self):
        username        = cherrypy.request.app.config['credentials']['username']
        password        = cherrypy.request.app.config['credentials']['password']
        host            = cherrypy.request.app.config['credentials']['host']
        database        = cherrypy.request.app.config['credentials']['database']
        poly2d_table    = cherrypy.request.app.config['credentials']['poly2d_table']
        cuboid_table    = cherrypy.request.app.config['credentials']['cuboid_table']
        world_id        = cherrypy.request.app.config['worldguard']['world_id']

        areas = compute.calculate_areas_from_db(username, password, host, database, poly2d_table, cuboid_table, world_id)
        if areas == -1:
            return "There's been an error, and we are unable to process your request."
        
        output = "<html><head><title>SimCity Areas</title></head><body><table>"
        for region in sorted(areas):
            output = output + "<tr><td>{region}</td><td>{area}</td></tr>".format(region=region, area=areas[region])

        return output + "</table></body></html>"

cherrypy.__version__ = ''
cherrypy._cperror._HTTPErrorTemplate = cherrypy._cperror._HTTPErrorTemplate.replace('Powered by <a href="http://www.cherrypy.org">CherryPy %(version)s</a>\n','%(version)s')
cherrypy.config.update("web.conf")
cherrypy.quickstart(AreaWebServer(), '/', "app.conf")